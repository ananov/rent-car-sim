<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <link rel="stylesheet" href="/css/homepage.css">
    <title>Rental Mobil SIM 4A</title>
  </head>
  <body>
    
    {{-- NAVBAR / HEADER--}}

    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
      <div class="container">
        <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
          <a class="navbar-brand" href="#"> <i class="fas fa-car"></i> Rental Mobil 4A</a>
        </div>
        <div class="navbar-collapse collapse w-100 order-2 dual-collapse2" id="navbarNavDropdown">
          <ul class="navbar-nav text-white">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Bantuan
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">Cara Sewa</a>
                <a class="dropdown-item" href="#">FAQ</a>
                <a class="dropdown-item" href="#">Ketentuan Layanan</a>
                <a class="dropdown-item" href="#">Syarat Ketentuan</a>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link">Hubungi | <i class="fas fa-phone-square-alt"></i> +62 812-3456-7890 |</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Masuk</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Daftar</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    {{-- CAROUSEL --}}

    <div id="carouselJumbotron" class="carousel slide" data-ride="carousel">
      <div class="carousel-caption d-none d-md-block">
        <h5>Rental Mobil 4A</h5>
        <p>Dapatkan pengalaman baru dalam menyewa mobil dengan mudah dan nyaman, tinggalkan cara konvensional.</p>
      </div>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="img/carousel/1.jpg" class="d-block w-100" alt="Jumbotron">
        </div>
        <div class="carousel-item">
          <img src="img/carousel/2.jpg" class="d-block w-100" alt="Jumbotron">
        </div>
        <div class="carousel-item">
          <img src="img/carousel/3.jpg" class="d-block w-100" alt="Jumbotron">
        </div>
        <div class="carousel-item">
          <img src="img/carousel/4.jpg" class="d-block w-100" alt="Jumbotron">
        </div>
        <div class="carousel-item">
          <img src="img/carousel/5.jpg" class="d-block w-100" alt="Jumbotron">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselJumbotron" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselJumbotron" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

    {{-- APA ITU RENTAL 4A --}}

    <div class="container apa-itu-bg mt-5">
      <div class="row">
        <div class="col-md-9">
          <h2 class="mb-3">Apa itu Rental Mobil 4A?</h2>
          <p>Rental Mobil 4A adalah website yang memberikan kemudahan untuk menyewa mobil untuk segala kebutuhanmu! Sewa Mobil dengan Pengemudi, Sewa Mobil Lepas Kunci, Pulang/Pergi Bandara, Semua di Rental 4A</p>
          <p>Penyewaan mobil kini sangat mudah dengan website Rental 4A, semua bisa kamu lakukan hanya dengan klik saja!</p>
        </div>
        <div class="col-md-3 text-center">
          <img src="/img/icons/car.png" alt="" class="img-fluid">
        </div>
      </div>
    </div>

    {{-- SEWA MOBIL POPULAR --}}

    <div class="container sewa-mobil-bg mb-5">
      <div class="row mt-5">
        <div class="col mb-2">
          <h4>Sewa Mobil Popular</h4>
        </div>
      </div>
      <div id="carouselMobil" class="carousel slide" data-ride="carousel">
        <div class="row">
          <div class="col one-mobil">
            <div class="text-block">
              <p>Toyota Avanza</p>
            </div>
            <img src="/img/mobil/avanza.jpg" alt="" class="img-fluid">
            <div class="text-right">
              <small>Start Form</small>
              <label class="font-weight-bold">IDR 420,000</label>
            </div>
          </div>
          <div class="col two-mobil">
            <div class="text-block">
              <p>Toyota Alphard</p>
            </div>
            <img src="/img/mobil/alphard.jpg" alt="" class="img-fluid">
            <div class="text-right">
              <small>Start Form</small>
              <label class="font-weight-bold">IDR 2,500,000</label>
            </div>
          </div>
          <div class="col three-mobil">
            <div class="text-block">
              <p>Honda BR-V</p>
            </div>
            <img src="/img/mobil/brv.jpg" alt="" class="img-fluid">
            <div class="text-right">
              <small>Start Form</small>
              <label class="font-weight-bold">IDR 450,000</label>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" id="prev-menu-mobil" href="#carouselMobil" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" id="next-menu-mobil" href="#carouselMobil" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>      
    </div>

    {{-- TESTIMONI --}}

    <div class="testimoni-bg">
      <div class="container">
        <div class="d-flex flex-row flex-wrap justify-content-center align-content-center">
          <div class="p-2 col text-center">
            <img src="/img/person/person.png" class="card-img-top rounded-circle mb-3" alt="...">
            <div class="card">
              <div class="card-body text-center">
                <h5 class="card-title font-weight-bold">Otong Surotong</h5>
                <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum, animi! Tenetur facere doloremque sit dolorum accusamus explicabo dicta nihil et distinctio, delectus non iure blanditiis dolores obcaecati, vero maiores quidem.</p>
                <p class="card-text text-warning">Menyewa Mobil Toyota Alphard</p>
              </div>
            </div>
          </div>
          <div class="p-2 col text-center">
            <img src="/img/person/person.png" class="card-img-top rounded-circle mb-3" alt="...">
            <div class="card">
              <div class="card-body text-center">
                <h5 class="card-title font-weight-bold">John Doe</h5>
                <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet nulla illo quos accusantium tenetur nostrum qui expedita repellendus nobis? Ipsa consectetur et voluptates fugiat harum recusandae, aperiam a debitis autem!</p>
                <p class="card-text text-warning">Menyewa Mobil Mercedes Benz</p>
              </div>
            </div>
          </div>
          <div class="p-2 col text-center">
            <img src="/img/person/person.png" class="card-img-top rounded-circle mb-3" alt="...">
            <div class="card">
              <div class="card-body text-center">
                <h5 class="card-title font-weight-bold">Hendra Setiawan</h5>
                <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perspiciatis optio est fuga odio? Magnam voluptate natus facere reiciendis, iste quas sed mollitia similique fuga, officiis atque ducimus? Eligendi, quam adipisci?</p>
                <p class="card-text text-warning">Menyewa Mobil Lamborghini</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- MENGAPA MEMILIH KAMI --}}

    <div class="mengapa-bg mt-5 mb-5">
      <div class="container text-center">
        <h3 class="mb-5">Mengapa Memilih Kami ?</h3>
        <div class="row text-center">
          <div class="col">
            <img src="/img/icons/globe.png" alt="">
            <h6 class="font-weight-bold mb-3">Hasil pencarian yang paling komprehensif</h6>
            <p>Cari dan bandingkan berbagai jenis mobil dengan harga lebih murah di Rental 4A.</p>
          </div>
          <div class="col">
            <img src="/img/icons/shield.png" alt="">
            <h6 class="font-weight-bold mb-3">Jaminan transaksi aman secara online</h6>
            <p>Transaksi Aman di Rental Mobil 4A dengan teknologi SSL dari Geotrust yang memastikan semua informasi. Aman dalam setiap transaksi</p>
          </div>
          <div class="col">
            <img src="/img/icons/card.png" alt="">
            <h6 class="font-weight-bold mb-3">Berbagai Cara Pembayaran</h6>
            <p>Rental Mobil 4A memberikan kenyamanan dalam bertransaksi dengan menyediakan beragam metode pembayaran yang mudah dan mudah.</p>
          </div>
          <div class="col">
            <img src="/img/icons/clock.png" alt="">
            <h6 class="font-weight-bold mb-3">24/7 Pelayanan pelanggan</h6>
            <p>Tim Rental Mobil 4A siap membantu Anda 24/7 dan juga dalam keadaan darurat. Rental Mobil 4A selalu siap membantu kamu!</p>
          </div>
        </div>
      </div>
    </div>

    {{-- FOOTER --}}

    <footer class="bg-dark text-white">
      <div class="container">
        <div class="row">
          <div class="col mt-5 mb-3 deskripsi">
            <h6 class="text-warning mb-3">Rental Mobil 4A - Rental Sewa Mobil Terbaik di Indonesia</h6>
            <p>Rental Mobil 4A adalah penyedia sewa mobil murah di Jakarta, Bandung, Bali, Jogjakarta, Bogor, Depok, Tangerang, Bekasi dan di Kota besar lainnya. Tersedia Aneka macam unit antara lain : Brio, Ayla, Calya, Sigra, Avanza, Xenia, BRV, Innova, Altis, Mercy ataupun Alphard. Anda juga bisa memilih ingin menggunakan Unit Matic atau Manual. Di Rental Mobil 4A anda bisa sewa mobil dengan supir ataupun lepas kunci, mulai dari Harian, Mingguan hingga Bulanan. Selain itu tersedia juga layanan lainnya seperti paket city tour, acara pernikahan Sampai antar jemput bandara.</p>
            <p>Reservasi atau pemesanan bisa dilakukan secara online melalui website Rental Mobil 4A atau via aplikasi Rental Mobil 4A di Playstore atau Apps Store. Dan Dapatkan Harga Terbaik. Harga Terjangkau. Unit Mobil Terbaru. Transaksi Aman. Booking Mudah.</p>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <p class="text-warning">Untuk informasi lebih lanjut</p>
            <p>+62 812-3456-7890</p>
            <p>Senin - Jum'at (08:00 - 17:00)</p>
            <p>Sabtu - Minggu (09:00 - 24:00)</p>
          </div>
          <div class="col bantuan">
            <p class="text-warning">Bantuan</p>
            <a href="#">Cara Sewa</a>
            <a href="#">FAQ</a>
            <a href="#">Ketentuan Layanan</a>
            <a href="#">Syarat Ketentuan</a>
          </div>
          <div class="col">
            <p class="text-warning">Tujuan terbaik</p>
            <p>Sewa Mobil di Bali</p>
            <p>Sewa Mobil di Bandung</p>
            <p>Sewa Mobil di Semarang</p>
            <p>Sewa Mobil di Jakarta</p>
            <p>Sewa Mobil di Yogyakarta</p>
          </div>
        </div>
        <div class="line-white"></div>
        <div class="row end-section">
          <div class="col">
            <p>© 2020 Kelompok 4A. Tugas Sistem Informasi Manajemen.</p>
          </div>
          <div class="col text-right">
            <a href="#"><i class="fab fa-twitter"></i></a>
            <a href="#"><i class="fab fa-instagram"></i></a>
            <a href="#"><i class="fab fa-facebook-f"></i></a>
            <a href="#"><i class="fab fa-whatsapp"></i></a>
          </div>
        </div>
      </div>
    </footer>

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/b4de44ce8a.js" crossorigin="anonymous"></script>
    <script src="js/homepage.js"></script>
  </body>
</html>